package chapter3;

import java.util.Scanner;

public class QuotaCalculator {
    public static void main(String[] args) {
        //initialize values we know
        int quota = 10;

        //get values we don't know
        System.out.println("How many sales did a saleman did?");
        Scanner scanner = new Scanner(System.in);
        int sales = scanner.nextInt();

        //make a decision on the path to take - output
        if (sales >= quota) {
            System.out.println("congratulations!");
        } else {
            int salesShort = quota - sales;
            System.out.println("you did not make your quota. You were " + salesShort + " sales short");
        }
    }
}
