package chapter3;

import java.util.Scanner;

public class LogicalOperatorLoanQualifier {

    public static void main(String[] args) {
        // initialize what we know
        int requiredSalary = 30000;
        int requiredWorkingYears = 2;

        //get what we dont
        System.out.println("How much you get paid (by year)");
        Scanner scanner = new Scanner(System.in);
        double userSalary = scanner.nextDouble();

        System.out.println("How many years do you work");
        int userYears = scanner.nextInt();
        scanner.close();

        //make decision
        if (userSalary >= requiredSalary && userYears >= requiredWorkingYears) {
            System.out.println("Congrats, you qualify for a loan!");
        } else {
            System.out.println("Sorry, you do not qualify for a loan");
        }

    }
}