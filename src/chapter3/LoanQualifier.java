package chapter3;

import java.util.Scanner;

public class LoanQualifier {
    public static void main(String[] args) {
        // initialize what we know
        int requiredSalary = 30000;
        int requiredWorkingYears = 2;

        //get what we dont
        System.out.println("How much you get paid (by year)");
        Scanner scanner = new Scanner(System.in);
        double userSalary = scanner.nextDouble();

        System.out.println("How many years do you work");
        int userYears = scanner.nextInt();
        scanner.close();

        //make decision
        if (userSalary >= requiredSalary) {
            if (userYears >= requiredWorkingYears) {
                System.out.println("Congrats! You qualify for the loan");
            }
            else {
                System.out.println("Sorry, you must have worked more at your current job"
                + requiredWorkingYears + " years.");
            }
        }
        else {
            System.out.println("Sorry, you must earn at least $" + requiredSalary + " to qualify for a loan");
        }

    }
}
