package chapter3;

import java.util.Scanner;

public class ChangeForADollarGame {
    public static void main(String[] args) {
        double penny = 0.01;
        double nickel = 0.05;
        double dimes = 0.10;
        double quarter = 0.25;

        // get the test score
        System.out.println("You goal is to enter enough change to make exactly $1.00");

        Scanner scanner = new Scanner(System.in);

        System.out.println("How many pennies would you like?");
        int userPenny = scanner.nextInt();

        System.out.println("How many nickles would you like?");
        int userNickel = scanner.nextInt();

        System.out.println("How many dimes would you like?");
        int userDimes = scanner.nextInt();

        System.out.println("How many quarters would you like?");
        int userQuarter = scanner.nextInt();

        scanner.close();

        double allTogether = (userPenny * penny) + (userNickel * nickel) + (userDimes * dimes) + (userQuarter * quarter);

        if (allTogether < 1) {
            double amountShort = 1 - allTogether;

            System.out.println("You were short " + String.format("%.2f", amountShort) + " cents.");
        }else if (allTogether > 1) {
            double amountOver = allTogether - 1;

            System.out.println("You were over " + String.format("%.2f", amountOver) + " cents.");
        } else {
            System.out.println("It's exactly $1.00! You win!");
        }

     }
}