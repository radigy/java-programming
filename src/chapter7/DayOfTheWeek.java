package chapter7;

import java.util.Scanner;

public class DayOfTheWeek {

    public static void main(String[] args) {

        String[] daysOfTheWeek = {"monday", "tuesday", "wednesday", "thursday", "friday","saturday", "sunday"};

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the number corresponding to the day of week");

        int number = scanner.nextInt();

        if (number < 7) {
            System.out.println("Your day is " + daysOfTheWeek[number - 1]);

        } else {
            System.out.println("There are no more days in week than 7");

        }

        scanner.close();
    }
}


