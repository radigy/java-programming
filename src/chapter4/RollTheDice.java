package chapter4;

import java.util.Random;

public class RollTheDice {
    public static void main(String[] args) {
        System.out.println("Roll 20 in 5 throws to win. Reach more than 20 or less than 20 to lose.");
        int userScore;
        int currentSpace = 0;

        for(int i=1; i<6; i++) {
            Random random = new Random();
            int die = random.nextInt(6) + 1;

            currentSpace = currentSpace + die;
            int wayToGo = 20 - currentSpace;

            if(currentSpace == 20) {
                System.out.println("Roll #" + i + " You have rolled a " + die + " You are now on space " + currentSpace + " Congrats! you win!");
                break;
            } else if (currentSpace > 20) {
                System.out.println("Roll #" + i + " You have rolled a " + die + " You reached over 20! You lose!");
                break;
            } else if (i==5 && currentSpace < 20) {
                System.out.println("Roll #" + i + " You have rolled a " + die + " You are now on space " + currentSpace + " There are now more rolls. You lose!");
            } else {
                System.out.println("Roll #" + i + " You have rolled a " + die + " You are now on space " + currentSpace + " You have " + wayToGo + " to go");
            }
        }


    }
}
