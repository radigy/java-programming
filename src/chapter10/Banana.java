package chapter10;

public class Banana extends Fruit {
    public void peel(){
        System.out.println("peel the banana");
    }

    public Banana() {
        setCalories(444);
        System.out.println("banana's calories " + calories);
    }

    @Override
    public void makeJuice() {
        System.out.println("banana juice");
    }

}
