package chapter10;

public class Apple extends Fruit{
    public void removeSeeds(){
        System.out.println("remove seeds");
    }

    public Apple() {
        setCalories(123);
        System.out.println("apple's calories " + calories);
    }

    @Override
    public void makeJuice() {
        System.out.println("apple juice");
    }

}
