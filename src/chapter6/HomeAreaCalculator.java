package chapter6;

public class HomeAreaCalculator{

    public static void main(String[] args) {
        //rectangle 1

        Rectangle room1 = new Rectangle();
        room1.setWidth(25);
        room1.setLength(50);
        double areOfRoom1 = room1.calculateArea();

        //rectangle 2
        Rectangle room2 = new Rectangle(30, 75);
        double areaOfRoom2 = room2.calculateArea();

        double totalArea = areOfRoom1 + areaOfRoom2;

        System.out.println("Area of both rooms: " + totalArea);

        


    }

}
