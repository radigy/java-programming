package chapter5;

public class PhoneBillRedo {

    private int id;
    private double baseCost;
    private int alottedMinutes;
    private int minutesUsed;

    public PhoneBillRedo() {
        id = 0;
        baseCost = 49.99;
        alottedMinutes = 100;
        minutesUsed = 110;
    }

    public PhoneBillRedo(int id){
        this.id = id;
        baseCost = 69.99;
        alottedMinutes = 800;
        minutesUsed = 800;
    }

    public PhoneBillRedo(int id, int baseCost, int alottedMinutes, int minutesUsed) {
        this.id = id;
        this.baseCost = baseCost;
        this.alottedMinutes = alottedMinutes;
        this.minutesUsed = minutesUsed;
    }

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBaseCost(){
        return baseCost;
    }

    public void setBaseCost(double baseCost) {
        this.baseCost = baseCost;
    }

    public int getAllottedMinutes(){
        return alottedMinutes;
    }
    public void setAlottedMinutes(int alottedMinutes){
        this.alottedMinutes = alottedMinutes;
    }

    public int getMinutesUsed(){
        return minutesUsed;
    }

    public void setMinutesUsed(int minutes){
        this.minutesUsed = minutes;
    }

    public double calculateOverage() {
        if (minutesUsed <= alottedMinutes) {
            return 0;
        }
        else {
            double overRate = 0.25;
            double overageMinutes = minutesUsed - alottedMinutes;
            return overageMinutes * overRate;
        }
    }

    public double calculateTax(){
        double taxRate = 0.15;
        return taxRate * (baseCost + calculateOverage());
    }

    public double calculateTotal(){
        return baseCost + calculateOverage() + calculateTax();
    }

    public void printItemizedBill(){
        System.out.println("ID: " + id);
        System.out.println("Base Rate: $" + baseCost);
        System.out.println("Overage Fee: $" + String.format("%.2f", calculateOverage()));
        System.out.println("Tax: $" + String.format("%.2f", calculateTax()));
        System.out.println("Total: $" + String.format("%.2f", calculateTotal()));
    }
}
