package chapter5;

import java.util.Scanner;

public class PhoneBillCalculator {

    static Scanner scanner = new Scanner(System.in);
    static double minutesFee = 0.25;

    public static void main(String[] args) {
        double planFee = getPlanFee();
        int overageMinutes = getOverageMinutes();
        double overcharge = getOvercharge(overageMinutes);
        double subtotal = planFee + overcharge;
        double tax = getTax(subtotal);
        double total = getTotal(planFee, overcharge, tax);

        System.out.println("Phone Bill Statement:");
        System.out.println("Plan: $" + planFee);
        System.out.println("Overcharge: $" + overcharge);
        System.out.println("Subtotal: $" + subtotal);
        System.out.println("Tax: $" + String.format("%.2f", tax));
        System.out.println("Total: $" + String.format("%.2f", total));
    }

    public static double getPlanFee() {
        System.out.println("Enter your plan fee:");
        double planFee = scanner.nextDouble();
        return planFee;
    }

    public static int getOverageMinutes() {
        System.out.println("Enter overage minutes:");
        int overageMinutes = scanner.nextInt();
        return overageMinutes;
    }

    public static double getOvercharge(int overchargeMinutes) {
        if (overchargeMinutes > 0) {
            double overcharge = overchargeMinutes * minutesFee;
            return overcharge;
        } else {
            double overcharge = 0;
            return overcharge;
        }
    }

    public static double getTax(double subtotal) {
        double tax = subtotal * (15.0f/100.0f);
        return tax;
    }

    public static double getTotal(double planFee, double overcharge, double tax) {
        double total = planFee + overcharge + tax;
        return total;
    }

}
