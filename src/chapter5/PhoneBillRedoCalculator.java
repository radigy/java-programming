package chapter5;

public class PhoneBillRedoCalculator {
    public static void main(String[] args) {
        PhoneBillRedo bill = new PhoneBillRedo();
        bill.setMinutesUsed(1000);
        bill.setId(12);
        bill.printItemizedBill();
    }
}
