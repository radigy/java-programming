package chapter2;

import java.util.Scanner;

public class Exercise {

    public static void main(String arg[]){

        Scanner scanner = new Scanner(System.in);

        System.out.println("What is your favourite season of the year? ");
        String season = scanner.nextLine();

        System.out.println("what is your favourite whole number? ");
        int wholeNumber = scanner.nextInt();

        System.out.println("what is your favourite adjective? ");
        String adjective = scanner.next();

        scanner.close();

        System.out.println("On a " + adjective + " " + season + " day, I drink a minimum of " + wholeNumber + " cups of coffee.");
    }
}
